package com.shiva.springsecurity.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shiva.springsecurity.model.Product;

public interface ProductRepo extends JpaRepository<Product, Integer> {

}
