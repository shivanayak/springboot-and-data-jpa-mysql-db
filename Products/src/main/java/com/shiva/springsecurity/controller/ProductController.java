package com.shiva.springsecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.shiva.springsecurity.dto.Coupon;
import com.shiva.springsecurity.model.Product;
import com.shiva.springsecurity.repo.ProductRepo;

@RestController
@RequestMapping(value = "/productapi")
public class ProductController {
	
	@Autowired
	private ProductRepo repo;
	
	@Autowired
	private RestTemplate template;
	
	@Value("${coupon.service.url}")
	private String couponService;
	
	@PostMapping("/product")
	public Product createProduct(@RequestBody Product product) {
		Coupon coupon=template.getForObject( couponService + product.getCode(), Coupon.class);
		product.setPrice(product.getPrice().subtract(coupon.getDiscount()));
		return repo.save(product);
		
	}

}
